# This program will collect all links to episodes in a paged rss feed.
# It will also work on feeds without pagination.
# This program is published under the terms of the GNU GPL Version 2.
# Copyright ahahn94 2017
#
# Usage:
# Just run
#           python3 main.py yourfeedurl
# and the links will be printed to the cli.
#
# If you run
#           python3 main.py
# you will be asked for the yourfeedurl. The links will be written to a file called "downloadList.txt"
#
# If you want to download the files directly from the cli, run
#           python3 main.py yourfeedurl | xargs wget
# Be aware that this can take some time, depending on the number of episodes and the connection speed.
#
# Tip:
# Run the code from the first example and paste the result into a Download Manager like JDownloader.
#
# Have fun!

import os
import sys
import feedparser
import urllib.request

interactive = False

# Main program
def main(feedUrl):
    nextPage = feedUrl  # Default to first page, so single paged feeds work
    lastPage = feedUrl  # same

    downloadTemp(nextPage)

    # Search for the link to the last page of the feed
    feed = feedparser.parse("temp.html")
    allRead = False
    for link in feed.feed.links:
        if (link.rel == "last"):
            lastPage = link.href
            if (interactive):
                print("Last Page: " + lastPage)

    if (interactive):
        # Writer for output file (downloadList.txt)
        writer = open("downloadList.txt", "w")

    # while the last page has not been processed
    while (allRead == False):
        downloadTemp(nextPage)

        if (interactive):
            print("Current Page: " + nextPage)

        readerTemp = open("temp.html", "r")

        lines = readerTemp.readlines()

        for i in range (0, len(lines)):
            line = lines[i]
            # The lines with the links to the episodes start with "<enclosure url"
            if  ("enclosure url" in line):
                s = line.split('"')
                if (interactive):
                    writer.write(s[1] + "\n")
                else:
                    print(s[1])

        readerTemp.close()

        if  (nextPage == lastPage):
            allRead = True

        nextPage = getNextPage()

    os.remove("temp.html")

    if (interactive):
        print("All done!")


# Download a temporary Copy of a page of the feed
def downloadTemp(nextPage):
    request = urllib.request.urlopen(nextPage)
    page = request.read().decode("utf-8")
    writerTemp = open("temp.html", "w")
    writerTemp.write(page)
    writerTemp.close()


# get the link to the next page of the feed
def getNextPage():
    feed = feedparser.parse("temp.html")
    for link in feed.feed.links:
        if (link.rel == "next"):
            return link.href



# Run the main program
if  (len(sys.argv)>1):
    podcastFeed = sys.argv[1]
    main(podcastFeed)
else:
    podcastFeed = input("Please enter the feed url: ")
    interactive = True
    main(podcastFeed)
